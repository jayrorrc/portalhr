CRM to:
- Manager employee
- Manager schedules
- Manager chart
- Manager Reports

-- db --
- PostgreSQL

-- Frontend --
- AngulaJS
- Jade

-- Internationalization --
- FormatJS

-- Unit Tests --
- Unit.js

-- Server --
- Express