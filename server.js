var app = require('./config/express')();

var server = app.listen(8081, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log("Listening http://%s:%s", host, port);
});