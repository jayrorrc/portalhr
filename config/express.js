var express = require('express');
var load = require('express-load');

module.exports = function() {

  var app = express();

  app.use(express.static('./public'));
  app.set('views', './views');
  app.set('view engine', 'jade');

  load('controllers')
  .into(app);

  return app;
}